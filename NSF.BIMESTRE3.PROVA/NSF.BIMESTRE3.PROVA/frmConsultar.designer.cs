﻿namespace NSF.Bimestre2.Prova.App
{
    partial class frmConsulta
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmConsulta));
            this.lblNome = new System.Windows.Forms.Label();
            this.picAstro = new System.Windows.Forms.PictureBox();
            this.lstAstros = new System.Windows.Forms.ListBox();
            this.lblDiametro = new System.Windows.Forms.Label();
            this.lblInfo = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.picAstro)).BeginInit();
            this.SuspendLayout();
            // 
            // lblNome
            // 
            this.lblNome.BackColor = System.Drawing.Color.Black;
            this.lblNome.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNome.ForeColor = System.Drawing.Color.White;
            this.lblNome.Location = new System.Drawing.Point(532, 243);
            this.lblNome.Name = "lblNome";
            this.lblNome.Size = new System.Drawing.Size(237, 38);
            this.lblNome.TabIndex = 0;
            this.lblNome.Text = "Estrela | Sol";
            this.lblNome.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // picAstro
            // 
            this.picAstro.Location = new System.Drawing.Point(530, 12);
            this.picAstro.Name = "picAstro";
            this.picAstro.Size = new System.Drawing.Size(241, 231);
            this.picAstro.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picAstro.TabIndex = 9;
            this.picAstro.TabStop = false;
            // 
            // lstAstros
            // 
            this.lstAstros.BackColor = System.Drawing.Color.Black;
            this.lstAstros.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lstAstros.ForeColor = System.Drawing.Color.White;
            this.lstAstros.FormattingEnabled = true;
            this.lstAstros.ItemHeight = 21;
            this.lstAstros.Location = new System.Drawing.Point(12, 383);
            this.lstAstros.Name = "lstAstros";
            this.lstAstros.Size = new System.Drawing.Size(208, 172);
            this.lstAstros.TabIndex = 14;
            this.lstAstros.SelectedIndexChanged += new System.EventHandler(this.lstAstros_SelectedIndexChanged);
            // 
            // lblDiametro
            // 
            this.lblDiametro.BackColor = System.Drawing.Color.Black;
            this.lblDiametro.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDiametro.ForeColor = System.Drawing.Color.White;
            this.lblDiametro.Location = new System.Drawing.Point(532, 281);
            this.lblDiametro.Name = "lblDiametro";
            this.lblDiametro.Size = new System.Drawing.Size(237, 37);
            this.lblDiametro.TabIndex = 15;
            this.lblDiametro.Text = "1.391.400 km (diâmetro)";
            this.lblDiametro.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblInfo
            // 
            this.lblInfo.BackColor = System.Drawing.Color.Black;
            this.lblInfo.Font = new System.Drawing.Font("Segoe UI", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblInfo.ForeColor = System.Drawing.Color.White;
            this.lblInfo.Location = new System.Drawing.Point(532, 318);
            this.lblInfo.Name = "lblInfo";
            this.lblInfo.Size = new System.Drawing.Size(237, 237);
            this.lblInfo.TabIndex = 16;
            this.lblInfo.Text = resources.GetString("lblInfo.Text");
            this.lblInfo.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // frmConsulta
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(781, 567);
            this.Controls.Add(this.lblInfo);
            this.Controls.Add(this.lblDiametro);
            this.Controls.Add(this.lstAstros);
            this.Controls.Add(this.picAstro);
            this.Controls.Add(this.lblNome);
            this.Name = "frmConsulta";
            this.Text = "Consulta de Astros";
            this.Load += new System.EventHandler(this.frmConsulta_Load);
            ((System.ComponentModel.ISupportInitialize)(this.picAstro)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label lblNome;
        private System.Windows.Forms.PictureBox picAstro;
        private System.Windows.Forms.ListBox lstAstros;
        private System.Windows.Forms.Label lblDiametro;
        private System.Windows.Forms.Label lblInfo;
    }
}

