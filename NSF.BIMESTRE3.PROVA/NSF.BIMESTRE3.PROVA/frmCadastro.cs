﻿using NSF.BIMESTRE3.PROVA.BUSINESS;
using NSF.BIMESTRE3.PROVA.MODELS.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NSF.Bimestre2.Prova.App
{
    public partial class frmCadastro : Form
    {
        public frmCadastro()
        {
            InitializeComponent();
        }
        string foto = "";
        private void btnSave_Click(object sender, EventArgs e)
        {
            //try
            //{
                string a = txtAstro.Text;
                string b = txtDiametro.Text;
                string c = txtInfo.Text;

                if (a == string.Empty || b == string.Empty || c == string.Empty)
                {
                    MessageBox.Show("Por favor, preencha todos os campos");
                }
                PlanetarioDTO plan = new PlanetarioDTO();
                plan.Nome = txtAstro.Text;
                plan.Info = txtInfo.Text;
                plan.Diametro = int.Parse(txtDiametro.Text);

                if (rbtEstrela.Checked)
                {
                    plan.Tipo = "Estrela";
                }
                else
                {
                    plan.Tipo = "Planeta";
                }

                byte[] byt = System.IO.File.ReadAllBytes(foto);
                string photo = Convert.ToBase64String(byt);
                plan.Imagem = photo;

                Planeta planeta = new Planeta();
                planeta.InserirPlaneta(plan);

            //}
            //catch (Exception ex)
            //{
                //MessageBox.Show(ex.Message);
            //}    
        }

        private void btnSelecionar_Click(object sender, EventArgs e)
        {
            OpenFileDialog janela = new OpenFileDialog();

            if (janela.ShowDialog() == DialogResult.OK)
            {
                foto = janela.FileName;
                picAstro.ImageLocation = janela.FileName;
            }
        }

        private void frmConsultar_Click(object sender, EventArgs e)
        {
            frmConsulta t = new frmConsulta();
            t.Show();
        }

        private void frmCadastro_Load(object sender, EventArgs e)
        {

        }
    }
}
