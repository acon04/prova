﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NSF.BIMESTRE3.PROVA.DATAACCESS.Database
{
    public class SistemaDatabase
    {
        public MySqlDataReader ExecuteSelect(string script, List<MySqlParameter> parameters)
        {
            Conexao conect = new Conexao();
            MySqlConnection conexao = conect.Create();

            MySqlCommand command = conexao.CreateCommand();
            command.CommandText = script;

            if (parameters != null)
            {
                foreach (MySqlParameter item in parameters)
                {
                    command.Parameters.Add(item);
                }
            }

            MySqlDataReader reader = command.ExecuteReader(System.Data.CommandBehavior.CloseConnection);
            return reader;
        }

        public void ExecuteInsert(string script, List<MySqlParameter> parameters)
        {
            Conexao conect = new Conexao();
            MySqlConnection conexao = conect.Create();

            MySqlCommand command = conexao.CreateCommand();
            command.CommandText = script;

            if (parameters != null)
            {
                foreach (MySqlParameter item in parameters)
                {
                    command.Parameters.Add(item);
                }
            }

            command.ExecuteNonQuery();
            conexao.Close();
        }
    }
}
