﻿using MySql.Data.MySqlClient;
using NSF.BIMESTRE3.PROVA.MODELS.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NSF.BIMESTRE3.PROVA.DATAACCESS.Database
{
    public class PlanetarioDatabase
    {
        public List<PlanetarioDTO> Listar()
        {
            string query = "SELECT * FROM tb_planetario";

            List<MySqlParameter> parameters = new List<MySqlParameter>();

            SistemaDatabase sist = new SistemaDatabase();

            MySqlDataReader reader = sist.ExecuteSelect(query, parameters);

            List<PlanetarioDTO> planetas = new List<PlanetarioDTO>();

            while (reader.Read())
            {
                PlanetarioDTO planeta = new PlanetarioDTO();

                planeta.Diametro = reader.GetInt32("nr_diametro");
                planeta.Imagem = reader.GetString("ds_img_planeta");
                planeta.Info = reader.GetString("ds_info");
                planeta.Nome = reader.GetString("nm_planeta");
                planeta.Tipo = reader.GetString("ds_tipo");

                planetas.Add(planeta);
            }

            return planetas;
        }

        public PlanetarioDTO BuscarPlaneta(string Nome)
        {
            string query = "SELECT * FROM tb_planetario WHERE nm_planeta = @Nome";

            List<MySqlParameter> parameters = new List<MySqlParameter>
                {
                    new MySqlParameter("@Nome", Nome)
                };

            SistemaDatabase sist = new SistemaDatabase();

            MySqlDataReader reader = sist.ExecuteSelect(query, parameters);

            PlanetarioDTO planeta = new PlanetarioDTO();

            if (reader.Read())
            {
                planeta.Diametro = reader.GetInt32("nr_diametro");
                planeta.Imagem = reader.GetString("ds_img_planeta");
                planeta.Info = reader.GetString("ds_info");
                planeta.Nome = reader.GetString("nm_planeta");
                planeta.Tipo = reader.GetString("ds_tipo");
            }

            return planeta;
        }

        public void InserirPlaneta(PlanetarioDTO planeta)
        {
            string query = "INSERT INTO tb_planetario (nm_planeta, nr_diametro, ds_img_planeta, ds_info, ds_tipo) VALUES (@Nome, @Diametro, @Img, @Info, @Tipo)";

            List<MySqlParameter> parameters = new List<MySqlParameter>
            {
                new MySqlParameter("@Nome",planeta.Nome),
                new MySqlParameter("@Diametro", planeta.Diametro),
                new MySqlParameter("@Img",planeta.Imagem),
                new MySqlParameter("@Info", planeta.Info),
                new MySqlParameter("@Tipo", planeta.Tipo)
            };

            SistemaDatabase sist = new SistemaDatabase();
            sist.ExecuteInsert(query, parameters);
        }
    }
}
