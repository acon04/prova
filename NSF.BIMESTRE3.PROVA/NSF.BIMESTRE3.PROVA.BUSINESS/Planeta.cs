﻿using NSF.BIMESTRE3.PROVA.DATAACCESS.Database;
using NSF.BIMESTRE3.PROVA.MODELS.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NSF.BIMESTRE3.PROVA.BUSINESS
{
    public class Planeta
    {
        public void InserirPlaneta(PlanetarioDTO planeta)
        {
            PlanetarioDatabase data = new PlanetarioDatabase();
            data.InserirPlaneta(planeta);
        }

        public PlanetarioDTO BuscarPlaneta(string Nome)
        {
            PlanetarioDatabase data = new PlanetarioDatabase();
            PlanetarioDTO planeta = new PlanetarioDTO();

            planeta = data.BuscarPlaneta(Nome);
            return planeta;
        }

        public List<PlanetarioDTO> Listar()
        {
            PlanetarioDatabase data = new PlanetarioDatabase();
            List<PlanetarioDTO> planeta = new List<PlanetarioDTO>();

            planeta = data.Listar();
            return planeta;
        }
    }
}
