﻿using NSF.BIMESTRE3.PROVA.BUSINESS;
using NSF.BIMESTRE3.PROVA.MODELS.DTO;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace NSF.Bimestre2.Prova.App
{
    public partial class frmConsulta : Form
    {
        public frmConsulta()
        {
            InitializeComponent();
        }

        private void frmConsulta_Load(object sender, EventArgs e)
        {
            Planeta planeta = new Planeta();

            List<PlanetarioDTO> planetas = new List<PlanetarioDTO>();

            planetas = planeta.Listar();

            foreach (PlanetarioDTO item in planetas)
            {
                lstAstros.Items.Add(item.Nome);
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            
        }

        private void lstAstros_SelectedIndexChanged(object sender, EventArgs e)
        {
            Planeta plan = new Planeta();

            PlanetarioDTO planeta = plan.BuscarPlaneta(lstAstros.SelectedItem.ToString());

            lblNome.Text = planeta.Tipo + " | ";
            lblDiametro.Text = Convert.ToString(planeta.Diametro);
            lblInfo.Text = planeta.Info;
            lblNome.Text = lblNome.Text + planeta.Nome;
            

            byte[] img = Convert.FromBase64String(planeta.Imagem);
            picAstro.Image = Image.FromStream(new System.IO.MemoryStream(img));
        }
    }
}
