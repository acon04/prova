﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NSF.BIMESTRE3.PROVA.MODELS.DTO
{
    public class PlanetarioDTO
    {
        public string Nome { get; set; }
        public int Diametro { get; set; }
        public string Imagem { get; set; }
        public string Info { get; set; }
        public string Tipo { get; set; }
    }
}
