﻿namespace NSF.Bimestre2.Prova.App
{
    partial class frmCadastro
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmCadastro));
            this.label1 = new System.Windows.Forms.Label();
            this.txtAstro = new System.Windows.Forms.TextBox();
            this.txtInfo = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtDiametro = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.picAstro = new System.Windows.Forms.PictureBox();
            this.btnSelecionar = new System.Windows.Forms.Button();
            this.btnSave = new System.Windows.Forms.Button();
            this.rbtEstrela = new System.Windows.Forms.RadioButton();
            this.rbtPlaneta = new System.Windows.Forms.RadioButton();
            this.frmConsultar = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.picAstro)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.BackColor = System.Drawing.Color.Black;
            this.label1.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.ForeColor = System.Drawing.Color.White;
            this.label1.Location = new System.Drawing.Point(77, 76);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 25);
            this.label1.TabIndex = 0;
            this.label1.Text = "Astro:";
            // 
            // txtAstro
            // 
            this.txtAstro.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtAstro.Location = new System.Drawing.Point(161, 73);
            this.txtAstro.Name = "txtAstro";
            this.txtAstro.Size = new System.Drawing.Size(201, 33);
            this.txtAstro.TabIndex = 1;
            this.txtAstro.Text = "Sol";
            // 
            // txtInfo
            // 
            this.txtInfo.Font = new System.Drawing.Font("Segoe UI", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtInfo.Location = new System.Drawing.Point(161, 151);
            this.txtInfo.Multiline = true;
            this.txtInfo.Name = "txtInfo";
            this.txtInfo.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.txtInfo.Size = new System.Drawing.Size(201, 145);
            this.txtInfo.TabIndex = 3;
            this.txtInfo.Text = resources.GetString("txtInfo.Text");
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.BackColor = System.Drawing.Color.Black;
            this.label2.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.ForeColor = System.Drawing.Color.White;
            this.label2.Location = new System.Drawing.Point(14, 154);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(128, 25);
            this.label2.TabIndex = 2;
            this.label2.Text = "Informações:";
            // 
            // txtDiametro
            // 
            this.txtDiametro.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDiametro.Location = new System.Drawing.Point(161, 112);
            this.txtDiametro.Name = "txtDiametro";
            this.txtDiametro.Size = new System.Drawing.Size(201, 33);
            this.txtDiametro.TabIndex = 5;
            this.txtDiametro.Text = "1391400";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.BackColor = System.Drawing.Color.Black;
            this.label3.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.ForeColor = System.Drawing.Color.White;
            this.label3.Location = new System.Drawing.Point(45, 115);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(100, 25);
            this.label3.TabIndex = 4;
            this.label3.Text = "Diâmetro:";
            // 
            // picAstro
            // 
            this.picAstro.Location = new System.Drawing.Point(532, 76);
            this.picAstro.Name = "picAstro";
            this.picAstro.Size = new System.Drawing.Size(201, 181);
            this.picAstro.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picAstro.TabIndex = 9;
            this.picAstro.TabStop = false;
            // 
            // btnSelecionar
            // 
            this.btnSelecionar.BackColor = System.Drawing.Color.Black;
            this.btnSelecionar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSelecionar.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSelecionar.ForeColor = System.Drawing.Color.White;
            this.btnSelecionar.Location = new System.Drawing.Point(532, 257);
            this.btnSelecionar.Name = "btnSelecionar";
            this.btnSelecionar.Size = new System.Drawing.Size(201, 33);
            this.btnSelecionar.TabIndex = 10;
            this.btnSelecionar.Text = "Selecione a Imagem";
            this.btnSelecionar.UseVisualStyleBackColor = false;
            this.btnSelecionar.Click += new System.EventHandler(this.btnSelecionar_Click);
            // 
            // btnSave
            // 
            this.btnSave.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.btnSave.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnSave.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnSave.ForeColor = System.Drawing.Color.White;
            this.btnSave.Location = new System.Drawing.Point(532, 498);
            this.btnSave.Name = "btnSave";
            this.btnSave.Size = new System.Drawing.Size(201, 33);
            this.btnSave.TabIndex = 11;
            this.btnSave.Text = "Salvar";
            this.btnSave.UseVisualStyleBackColor = false;
            this.btnSave.Click += new System.EventHandler(this.btnSave_Click);
            // 
            // rbtEstrela
            // 
            this.rbtEstrela.AutoSize = true;
            this.rbtEstrela.BackColor = System.Drawing.Color.Black;
            this.rbtEstrela.Checked = true;
            this.rbtEstrela.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtEstrela.ForeColor = System.Drawing.Color.White;
            this.rbtEstrela.Location = new System.Drawing.Point(274, 37);
            this.rbtEstrela.Name = "rbtEstrela";
            this.rbtEstrela.Size = new System.Drawing.Size(88, 29);
            this.rbtEstrela.TabIndex = 13;
            this.rbtEstrela.TabStop = true;
            this.rbtEstrela.Text = "Estrela";
            this.rbtEstrela.UseVisualStyleBackColor = false;
            // 
            // rbtPlaneta
            // 
            this.rbtPlaneta.AutoSize = true;
            this.rbtPlaneta.BackColor = System.Drawing.Color.Black;
            this.rbtPlaneta.Font = new System.Drawing.Font("Segoe UI", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.rbtPlaneta.ForeColor = System.Drawing.Color.White;
            this.rbtPlaneta.Location = new System.Drawing.Point(164, 37);
            this.rbtPlaneta.Name = "rbtPlaneta";
            this.rbtPlaneta.Size = new System.Drawing.Size(96, 29);
            this.rbtPlaneta.TabIndex = 12;
            this.rbtPlaneta.Text = "Planeta";
            this.rbtPlaneta.UseVisualStyleBackColor = false;
            // 
            // frmConsultar
            // 
            this.frmConsultar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(64)))), ((int)(((byte)(0)))));
            this.frmConsultar.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.frmConsultar.Font = new System.Drawing.Font("Segoe UI", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.frmConsultar.ForeColor = System.Drawing.Color.White;
            this.frmConsultar.Location = new System.Drawing.Point(50, 498);
            this.frmConsultar.Name = "frmConsultar";
            this.frmConsultar.Size = new System.Drawing.Size(201, 33);
            this.frmConsultar.TabIndex = 14;
            this.frmConsultar.Text = "Consultar";
            this.frmConsultar.UseVisualStyleBackColor = false;
            this.frmConsultar.Click += new System.EventHandler(this.frmConsultar_Click);
            // 
            // frmCadastro
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(781, 567);
            this.Controls.Add(this.frmConsultar);
            this.Controls.Add(this.rbtEstrela);
            this.Controls.Add(this.rbtPlaneta);
            this.Controls.Add(this.btnSave);
            this.Controls.Add(this.btnSelecionar);
            this.Controls.Add(this.picAstro);
            this.Controls.Add(this.txtDiametro);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtInfo);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtAstro);
            this.Controls.Add(this.label1);
            this.Name = "frmCadastro";
            this.Text = "Cadastro de Astros";
            this.Load += new System.EventHandler(this.frmCadastro_Load);
            ((System.ComponentModel.ISupportInitialize)(this.picAstro)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox txtAstro;
        private System.Windows.Forms.TextBox txtInfo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtDiametro;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.PictureBox picAstro;
        private System.Windows.Forms.Button btnSelecionar;
        private System.Windows.Forms.Button btnSave;
        private System.Windows.Forms.RadioButton rbtEstrela;
        private System.Windows.Forms.RadioButton rbtPlaneta;
        private System.Windows.Forms.Button frmConsultar;
    }
}

