﻿using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NSF.BIMESTRE3.PROVA.DATAACCESS.Database
{
    public class Conexao
    {
        public MySqlConnection Create()
        {
            string connectionString = "server=localhost;database=planetariodb;uid=root;pwd=soso";

            MySqlConnection con = new MySqlConnection(connectionString);
            con.Open();

            return con;
        }
    }
}
